package randomstracker;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class FileUploader {
	// public static void uploadFile(File file, String name) throws
	// ClientProtocolException, IOException{
	// HttpClient httpclient = new DefaultHttpClient();
	// httpclient.getParams().setParameter(CoreProtocolPNames.PROTOCOL_VERSION,
	// HttpVersion.HTTP_1_1);
	//
	// HttpPost httppost = new
	// HttpPost("http://18.239.7.196/upload.php?username=" + name);
	//
	// MultipartEntity mpEntity = new MultipartEntity();
	// ContentBody cbFile = new FileBody(file);
	// mpEntity.addPart("upfile", cbFile);
	//
	// httppost.setEntity(mpEntity);
	// System.out.println("executing request " + httppost.getRequestLine());
	// HttpResponse response = httpclient.execute(httppost);
	// HttpEntity resEntity = response.getEntity();
	//
	// System.out.println(response.getStatusLine());
	// if (resEntity != null) {
	// System.out.println(EntityUtils.toString(resEntity));
	// }
	// if (resEntity != null) {
	// resEntity.consumeContent();
	// }
	//
	// httpclient.getConnectionManager().shutdown();
	// }
	public static void uploadFile(File file, String name)
			throws UnknownHostException, IOException {
		Socket socket = new Socket("18.239.7.196", 57684);

		BufferedOutputStream bs = new BufferedOutputStream(
				socket.getOutputStream());
		PrintWriter pw = new PrintWriter(bs);
		FileInputStream fis = new FileInputStream(file);
		BufferedInputStream bis = new BufferedInputStream(fis);

		pw.println(file.getName());
		pw.println(name);
		pw.flush();

		byte[] buffer = new byte[1024];
		int read;
		int total = 0;
		while ((read = bis.read(buffer)) > 0) {
			bs.write(buffer, 0, read);
			total += read;
		}

		System.out.println("Wrote " + total + " bytes");

		bs.flush();
		bs.close();
		pw.close();
		fis.close();
		bis.close();
		socket.close();
	}
}