package randomstracker;

import java.awt.geom.Area;
import java.awt.geom.PathIterator;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;

import javax.imageio.ImageIO;

import org.osbot.BotApplication;
import org.osbot.rs07.api.model.NPC;
import org.osbot.rs07.api.model.RS2Object;
import org.osbot.rs07.api.ui.RS2Interface;
import org.osbot.rs07.api.ui.RS2InterfaceChild;
import org.osbot.rs07.script.Script;

/**
 * RandomTracker monitors the random executor, and screenshots the canvas when a
 * random event appears. It also records interface and model data
 * 
 * @author Ericthecmh
 * 
 */
public class RandomTracker implements Runnable {

	// Reference to the script being run
	private final Script script;
	// Default storage location for the image captures
	private final String destination;

	// Currently solving random
	private boolean solving;
	// Start time
	private long startTime;
	// Whether or not to capture and upload data
	private boolean shouldScreenshot = false;

	public RandomTracker(Script s) {
		script = s;
		String separator = System.getProperty("file.separator");
		destination = System.getProperty("user.home") + separator + "OSBot"
				+ separator + "Scripts" + separator;
	}

	@Override
	public void run() {
		try {
			Thread.sleep(100);
		} catch (Exception e) {

		}
		while (script.getBot().getScriptExecutor().getCurrent() != null) {
			try {
				String randomName = null;
				if (script.getBot().getRandomExecutor().getCurrent() != null) {
					randomName = script.getBot().getRandomExecutor()
							.getCurrent().getName();
					if (!solving) {
						// Now solving, mark as solving
						solving = true;
						startTime = System.currentTimeMillis();
						shouldScreenshot = true;
					} else {
						if (System.currentTimeMillis() - startTime > 5 * 60 * 1000) {
							// Taking too long to solve, disable screenshot
							shouldScreenshot = false;
						}
					}
				} else {
					solving = false;
					shouldScreenshot = false;
				}

				if (shouldScreenshot) {
					long ID = -1;

					try {
						ID = captureScreen(randomName);
					} catch (Exception e) {
						e.printStackTrace();
					}

					if (ID != -1) {
						final File imageFile = new File(destination
								+ randomName + ID + "Image.png");
						final File npcFile = new File(destination + randomName
								+ ID + "NPCS.txt");
						final File objectFile = new File(destination
								+ randomName + ID + "OBJECTS.txt");
						final File interfaceFile = new File(destination
								+ randomName + ID + "Interfaces.txt");
						// Upload the files
						new Thread(new Runnable() {
							public void run() {
								try {
									try {
										FileUploader
												.uploadFile(
														imageFile,
														BotApplication
																.getInstance()
																.getOSAccount().username);
										FileUploader
												.uploadFile(
														npcFile,
														BotApplication
																.getInstance()
																.getOSAccount().username);
										FileUploader
												.uploadFile(
														objectFile,
														BotApplication
																.getInstance()
																.getOSAccount().username);
										FileUploader
												.uploadFile(
														interfaceFile,
														BotApplication
																.getInstance()
																.getOSAccount().username);
									} catch (Exception e) {

									}
									try {
										// Delete the files to free up space
										imageFile.delete();
										npcFile.delete();
										objectFile.delete();
										interfaceFile.delete();
									} catch (Exception e) {

									}
								} catch (Exception e) {

								}
							}
						}).start();
					}
				}
			} catch (Exception e) {

			}
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {

			}
		}
	}

	/**
	 * Makes a screenshot and records all of the interface coordinate data (and
	 * disabled media IDs for each interface child)
	 * 
	 * @param name
	 *            Name of the random executor
	 * 
	 * @return time of request, millisecond precision
	 * @throws Exception
	 */
	public long captureScreen(String name) throws Exception {
		// Time captureScreen was called
		long requestTime = System.currentTimeMillis();

		BufferedImage image = script.getBot().getCanvas().getGameBuffer();

		File interfacesFile = new File(destination + name + requestTime
				+ "Interfaces.txt");
		File npcsFile = new File(destination + name + requestTime + "NPCS.txt");
		File objectsFile = new File(destination + name + requestTime
				+ "OBJECTS.txt");

		PrintStream ps = new PrintStream(interfacesFile);
		boolean[] validInterfaces = script.interfaces.getValid();
		for (int parentId = 0; parentId < validInterfaces.length; parentId++) {
			RS2Interface parent = script.interfaces.get(parentId);
			if (parent == null)
				continue;
			else {
				for (RS2InterfaceChild child : parent.getChildren()) {
					if (child == null)
						continue;
					if (child.isVisible()) {
						if (child.getChildren() == null
								|| child.getChildren().length == 0) {
							ps.println("2"); // Level
							ps.println(child.getParentId());
							ps.println(child.getId());
							ps.println((int) child.getRectangle().getMinX());
							ps.println((int) child.getRectangle().getMinY());
							ps.println((int) child.getRectangle().getMaxX());
							ps.println((int) child.getRectangle().getMaxY());
							ps.println((int) child.getDisabledMediaId());
						} else {
							for (RS2InterfaceChild sub : child.getChildren()) {
								ps.println("3");
								ps.println(parent.getId());
								ps.println(child.getId());
								ps.println(sub.getId());
								ps.println((int) sub.getRectangle().getMinX());
								ps.println((int) sub.getRectangle().getMinY());
								ps.println((int) sub.getRectangle().getMaxX());
								ps.println((int) sub.getRectangle().getMaxY());
								ps.println((int) sub.getDisabledMediaId());
							}
						}
					}
				}
			}
		}
		ps.close();

		ps = new PrintStream(npcsFile);
		for (NPC npc : script.npcs.getAll()) {
			if (npc == null || !npc.getPosition().isVisible(script.bot))
				continue;
			ps.println(npc.getName());
			ps.println(npc.getId());
			ps.println(npc.getX());
			ps.println(npc.getY());
			ps.println(npc.getZ());
			for (int i : npc.getModelIds()) {
				ps.println(i);
			}
			ps.println("DONE");
			Area area = npc.getModel().getArea(npc.getGridX(), npc.getGridY(),
					script.myPlayer().getZ());
			if (area != null) {
				PathIterator it = area.getPathIterator(null);
				while (!it.isDone()) {
					float[] coords = new float[2];
					int type = it.currentSegment(coords);
					ps.println(type);
					ps.println((int) coords[0]);
					ps.println((int) coords[1]);
					it.next();
				}
			}
			ps.println("DONE");
		}
		ps.close();

		ps = new PrintStream(objectsFile);
		for (RS2Object object : script.objects.getAll()) {
			if (object == null || !object.getPosition().isVisible(script.bot)
					|| object.getName() == null)
				continue;
			ps.println(object.getName());
			ps.println(object.getId());
			ps.println(object.getX());
			ps.println(object.getY());
			ps.println(object.getZ());
			for (int i : object.getModelIds()) {
				ps.println(i);
			}
			ps.println("DONE");
			Area area = object.getModel().getArea(object.getGridX(),
					object.getGridY(), script.myPlayer().getZ());
			if (area != null) {
				PathIterator it = area.getPathIterator(null);
				while (!it.isDone()) {
					float[] coords = new float[2];
					int type = it.currentSegment(coords);
					ps.println(type);
					ps.println((int) coords[0]);
					ps.println((int) coords[1]);
					it.next();
				}
			}
			ps.println("DONE");
		}
		ps.close();

		File file = new File(destination + name + requestTime + "Image.png");
		file.getParentFile().mkdirs();
		ImageIO.write(image, "png", file);

		return requestTime;
	}

}
